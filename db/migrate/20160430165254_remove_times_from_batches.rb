class RemoveTimesFromBatches < ActiveRecord::Migration
  def change
    remove_column :batches, :batch_start_time, :datetime
    remove_column :batches, :batch_end_time, :datetime
  end
end
