class AddTimesToBatches < ActiveRecord::Migration
  def change
    add_column :batches, :batch_start_date, :date
    add_column :batches, :batch_end_date, :date
    add_column :batches, :batch_start_time, :time
    add_column :batches, :batch_end_time, :time
  end
end
