class RemoveCourseIdFromBatches < ActiveRecord::Migration
  def change
    remove_column :batches, :course_id, :string
  end
end
