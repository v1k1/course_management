class CreateBatches < ActiveRecord::Migration
  def change
    create_table :batches do |t|
      t.string :name
      t.integer :course_id
      t.boolean :active
      t.datetime :batch_start_time
      t.datetime :batch_end_time

      t.timestamps null: false
    end
  end
end
