class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :first_name
      t.string :last_name_string
      t.string :email
      t.string :mobile_number
      t.string :address_line_one
      t.string :address_line_two
      t.string :city
      t.string :state
      t.string :country
      t.string :pincode
      t.date :birthdate

      t.timestamps null: false
    end
  end
end
