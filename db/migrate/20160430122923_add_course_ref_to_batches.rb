class AddCourseRefToBatches < ActiveRecord::Migration
  def change
    add_reference :batches, :course, index: true, foreign_key: true
  end
end
