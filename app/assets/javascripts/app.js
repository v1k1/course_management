angular.module('cms', ['ngRoute', 'templates', 'Devise'])
.config(['$routeProvider',
	function($routeProvider){
		$routeProvider
			.when('/login',{
				templateUrl: 'auth/_login.html',
      			controller: 'AuthCtrl'
			})
			.when('/register',{
				templateUrl: 'auth/_register.html',
      			controller: 'AuthCtrl'
			})
			.when('/home',{
				templateUrl: '/home.html',
      			controller: 'MainCtrl'
			})
			.when('/students/create',{
				templateUrl: 'students/_student_create.html',
				controller: 'studentsCtrl',
				resolve: {
					all_students: function(studentService){
						return studentService.getAll();
					}
				}
			})
			.when('/students',{
				templateUrl: 'students/_students_show.html',
				controller: 'studentsCtrl',
				resolve: {
					all_students: function(studentService){
						return studentService.getAll();
					}
				}
			})
			.when('/courses',{
				templateUrl: 'courses/_courses.html',
    			controller: 'coursesCtrl',
    			resolve: {
	    			all_courses: function(courseService){
	    				return courseService.getAll();
	    			}
 				}
			})
			.when('/courses/create',{
				templateUrl: 'courses/_course_create.html',
		    	controller: 'coursesCtrl',
		    	resolve: {
	    			all_courses: function(courseService){
	    				return courseService.getAll();
	    			}
 				}
			})
			.when('/courses/show/:id',{
				templateUrl: 'courses/_course_show.html',
		    	controller: 'showCourseCtrl',
		    	resolve: {
	    			single_course: function(courseService, $route){
	    				return courseService.getCourse($route.current.params.id);
	    			},
	    			batch_list: function(batchService, $route){
	    				return batchService.getBachesForCourse($route.current.params.id);
	    			}
 				}
			})
			.when('/batch/show/:id',{
				templateUrl: 'batches/_batch_show.html',
		    	controller: 'showBatchCtrl',
		    	resolve: {
		    		batch: function(batchService, $route){
		    			return batchService.getBatch($route.current.params.id);
		    		},
		    		all_students: function(studentService){
						return studentService.getAll();
					},
					enrolled_students: function(batchService, $route){
						return batchService.getEnrolledStudents($route.current.params.id);
					}
		    	}
			})
			.when('/batches/:id',{
				templateUrl: 'batches.html',
		    	controller: 'showBatchesCtrl',
			})
			.when('/courses/:course_id/batch/create',{
				templateUrl: 'batches/_batch_create.html',
		    	controller: 'createBatcheCtrl',
			})
	}
])
.factory('batchService',[
	'$http',
	'$location',
	function($http, $location){
		var f = {};
		f.getBachesForCourse = function(id){
			var promise = $http({
				method: 'GET',
				url: '/v1/courses/'+id+'/batches'
			});
			promise.success(function(data, status, headers, conf){
				return data;
			});

			return promise;
		}
		f.createBatch = function(batch){
			return $http.post('/v1/courses/'+batch.course_id+'/batches',batch)
			.then(function(data){
				alert('Batch created!')
				$location.path('/courses/show/'+batch.course_id)
			}, function(error){
				alert('Error: '+JSON.stringify(error));
			})
		}
		f.getBatch = function(id){
			var promise = $http({
				method: 'GET',
				url: '/v1/batches/'+id
			});

			promise.success(function(data, status, headers, conf){
				return data;
			});

			return promise;
		}
		f.getEnrolledStudents = function(id){
			var promise = $http({
				method: 'GET',
				url: '/v1/batch/'+id+'/students'
			});

			promise.success(function(data, status, headers, conf){
				return data;
			});

			return promise;
		}
		return f;
	}
])
.factory('studentService',[
	'$http',
	'$location',
	function($http, $location){
		var s = {};
		s.createStudent = function(student){
			return $http.post('/v1/students',student).success(function(){
				alert('student created!')
				$location.route('/students')
			})
		}
		s.getAll = function(){
			var promise = $http({
				method: 'GET',
				url: '/v1/students'
			});

			promise.success(function(data, status, headers, conf){
				return data;
			})
			return promise;
		}
		return s;
	}])
.controller('studentsCtrl',[
	'$scope', 
	'studentService',
	'all_students',
	function($scope, studentService, all_students){
		$scope.all_students = all_students.data;
		$scope.createStudent = function(){
			var student = {
				first_name: $scope.first_name,
				last_name_string: $scope.last_name,
				//birthdate: $scope.birth_date,
				email: $scope.email
			};
			studentService.createStudent(student);
		}
}])
.controller('showBatchesCtrl',[
	'$scope',
	'batchService',
	'$routeParams',
	function($scope, batchService, $routeParams){
		$scope.batches = batchService.getBachesForCourse([$routeParams.id]);
		$scope.create = function(){
			alert('hi')
		}
	}
])
.controller('showBatchCtrl',[
	'$scope',
	'batch',
	'$http',
	'enrolled_students',
	'all_students',
	'$routeParams',
	function($scope, batch, $http, enrolled_students, all_students, $routeParams){
		$scope.batch = batch.data;
		$scope.enrolled_students = enrolled_students.data;
		$scope.all_students = all_students.data;
		$scope.enrollStudent = function(student_id){
			$http.post('/v1/batch/'+$scope.batch.id+'/register/'+student_id)
			.then(function(data){
				var found_student = $scope.all_students.find(function(s){return s.id == student_id})
				$scope.enrolled_students.push(found_student);
				alert('Student Enrolled!');
			}, function(error){
				alert('Error: '+JSON.stringify(error));
			})
		}
		$scope.unenrollStudent = function(student){
			$http.delete('/v1/batch/'+$scope.batch.id+'/register/'+student.id)
			.then(function(data){
  				var index = $scope.enrolled_students.indexOf(student);
  				$scope.enrolled_students.splice(index, 1);     
			}, function(error){
				alert('Error: '+JSON.stringify(error));
			})
		}
	}
])
.controller('createBatcheCtrl',[
	'$scope',
	'batchService',
	'$routeParams',
	function($scope, batchService, $routeParams){
		$scope.course_id=$routeParams.course_id;
		$scope.createBatch = function(){
			if(!$scope.batch_name || $scope.batch_name === '') {return;}
			var batch = {
				name: $scope.batch_name,
				batch_start_date: $scope.batch_start_date,
				batch_end_date: $scope.batch_end_date,
				batch_start_time: $scope.batch_start_time,
				batch_end_time: $scope.batch_end_time,
				course_id: $scope.course_id
			};
			batchService.createBatch(batch);
			
			$scope.batch_name='';
			$scope.batch_start_date='';
			$scope.batch_end_date='';
			$scope.batch_start_time='';
			$scope.batch_end_time='';
		}
	}
])
.controller('MainCtrl', [
	'$scope', 'posts',
	function($scope, posts){
		$scope.test = 'Hello World!';
		$scope.posts = posts.posts;
		$scope.addPost = function(){
			if( !$scope.title || $scope.title === '') return;
			$scope.posts.push({
				title: $scope.title,
				link: $scope.link, 
				upvotes: 0,
				comments: [
					{author: 'Joe', body: 'Cool post!', upvotes: 0},
					{author: 'Bob', body: 'Great idea but everything is wrong!', upvotes: 0}
				]
			});
			$scope.title = '';
			$scope.link = '';
		};
		$scope.incementUpvote = function(post){
			post.upvotes+=1;
		}
	}
])
.factory('courseService',[
	'$http',
	'$location',
	function($http, $location){
		var o = {};
		o.getAll = function(){
			/*alert('getAll');
			return $http.get('/v1/courses.json').success(function(data){
				angular.copy(data, o.courses);
			})*/

			var promise = $http({
				method: 'GET',
				url: '/v1/courses.json'
			});

			promise.success(function(data, status, headers, conf){
				return data;
			});
			return promise;
		}
		o.getCourse = function(course_id){
			var promise = $http({
				method: 'GET',
				url: '/v1/courses/'+course_id
			});

			promise.success(function(data, status, headers, conf){
				return data;
			});
			return promise;
		}
		o.createCourse = function(course){
			return $http.post('/v1/courses',course).success(function(data){
				alert('Course Created!')
				$location.path( "/courses" );
			})
		}
		return o;
	}
])
.controller('showCourseCtrl',[
	'$scope',
	'single_course',
	'batch_list',
	'courseService',
	function($scope, single_course, batch_list, courseService){
		$scope.course = single_course.data;
		$scope.batch_list = batch_list.data;
	}
])
.controller('coursesCtrl',[
	'$scope',
	'all_courses',
	'courseService',
	function($scope, all_courses, courseService){
		$scope.courses = all_courses.data;
		$scope.createCourse = function(){
			if(!$scope.name || $scope.name === '') {return;}

			courseService.createCourse({
				name: $scope.name,
				description: $scope.description
			});
			$scope.name='';
			$scope.description='';
		}
		$scope.show = function($scope, $routeParams, courseService){
			alert($routeParams.id)
		}
	}
]);
