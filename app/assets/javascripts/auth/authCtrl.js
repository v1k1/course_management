angular.module('cms')
.controller('AuthCtrl', [
	'$scope',
	'$location',
	'Auth',
	function($scope, $location, Auth){
		$scope.login = function(){
			Auth.login($scope.user).then(function(){
				$location.path( "/courses" );
			}, function(error){
				alert(JSON.stringify(error))
			});
		};

		$scope.register = function(){
			Auth.register($scope.user).then(function(){
				$location.path( "/courses" );
			}, function(error){
				alert(JSON.stringify(error))
			});
		};
	}
]);