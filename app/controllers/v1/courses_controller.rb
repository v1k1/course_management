class V1::CoursesController < ApplicationController
	before_filter :authenticate_user!, only: [:index, :create, :upvote]
	def index
		respond_with Course.all
	end

	def create
		respond_with :v1, Course.create(post_params)
	end

	def show
		respond_with :v1, Course.find_by(id: params[:id])
	end

	private
  	def post_params
    	params.require(:course).permit(:name, :description)
  	end
end
