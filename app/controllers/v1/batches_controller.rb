class V1::BatchesController < ApplicationController
	before_filter :authenticate_user!, only: [:index, :create, :show, :register, :unregister, :students]
	def create
		respond_with :v1, Batch.create(post_params)
	end

	def index
		respond_with :v1, Batch.where(course_id: params[:course_id])
	end

	def show
		respond_with :v1, Batch.find_by(id: params[:id])
	end

	def register
		batch_id = params[:id]
		student_id = params[:student_id]
		already_registered = Registration.find_by({student_id: student_id, batch_id: batch_id})
		if( already_registered != nil )
			render status: 500, json: {error: 'Already Registered'}
			return
		end
		puts 'yolo'
		puts already_registered.inspect
		Registration.create({student_id: student_id, batch_id: batch_id})
		render status: 200, json: @controller.to_json
	end

	def unregister
		batch_id = params[:id]
		student_id = params[:student_id]
		respond_with :v1, Registration.find_by({student_id: student_id, batch_id: batch_id}).delete
	end

	def students
		batch_id = params[:id]
		batch = Batch.find_by(id: batch_id)
		respond_with :v1, batch.students
	end

	private
  	def post_params
    	params.require(:batch).permit(:name, :batch_start_date, :batch_end_date, :batch_start_time, :batch_end_time, :course_id)
  	end
end
