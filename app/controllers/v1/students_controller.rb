class V1::StudentsController < ApplicationController
	before_filter :authenticate_user!, only: [:index, :create, :show]
	def index
		respond_with Student.all
	end

	def create
		respond_with :v1, Student.create(post_params)
	end

	def show
		respond_with :v1, Student.find_by(id: params[:id])
	end

	private
  	def post_params
    	params.require(:student).permit(:first_name, :last_name_string, :email, :mobile_number, :address_line_one, :address_line_two, :city, :state, :country, :pincode, :birthdate)
  	end
end
