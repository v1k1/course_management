class Student < ActiveRecord::Base
	has_many :batches, through: :registrations
	has_many :registrations
end
