class Batch < ActiveRecord::Base
	belongs_to :courses
	has_many :students, through: :registrations
	has_many :registrations
end
