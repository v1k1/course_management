# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Course Management System
* Version 1.0.0

### Current Functionality ###
* Authentication/ user registration
* Course creation.
* Students creation.
* Creating Batches for a course.
* Enrolling/Un enrolling students from a particular batch.

### Short comings or TODO list  ###
* integration with Postgres DB
* splitting and logically groping controllers in app.js (Angular)
* implementing/ improving client side caching.
* figuring out a good image store for course, students image uploads.
* for this version i have disabled CORS. due to conflict with angular. enabling it.
* Configuring device (authentication) for token based authentication.
* Right now there is partial data validation done by UI. Need to put Rails Model level validation.
### How do I get set up? ###

* install ruby
* install rails
* install sqlite3
* clone project
* bundle install
* ./bin/rake db:migrate
* rails s

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact