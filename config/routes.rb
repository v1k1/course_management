Rails.application.routes.draw do

  root to: 'application#angular'
  devise_for :users
  namespace :v1 do
    resources :courses, defaults: { format: 'json' } do
      resources :batches, shallow: true, defaults: { format: 'json' }
    end
    
    get 'students' => 'students#index', defaults: { format: 'json' }
    post 'students' => 'students#create', defaults: { format: 'json' }
    get 'student/:id/batches' => 'students#batches', defaults: { format: 'json' }
    
    get 'instructors' => 'instructors#index', defaults: { format: 'json' }
    get 'instructor/:id/batches' => 'instructors#batches', defaults: { format: 'json' }

    post 'batch/:id/register/:student_id' => 'batches#register', defaults: { format: 'json' }
    get 'batch/:id/students' => 'batches#students', defaults: { format: 'json' }
    get 'batch/:id/nonstudents' => 'batches#students', defaults: { format: 'json' }
    post 'batch/:id/assign/:instructor_id' => 'batches#assign', defaults: { format: 'json' }
    delete 'batch/:id/register/:student_id' => 'batches#unregister', defaults: { format: 'json' }
    delete 'batch/:id/assign/:instructor_id' => 'batches#unassign', defaults: { format: 'json' }
  end
  

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
